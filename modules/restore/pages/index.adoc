= Backup/Restore

The most easy backend uses simple files to store all certificates,
keys, and other data.

== Backup

To backup the installation halt the instance (kbd:[Ctrl+C] or send
signal `TERM` to the `node-red` process) and perform a backup,

[tabs]
====
Simple tar::
+
--
The following command creates a backup using the `tar(1)` command-line utility:

[source,shell]
----
# umask 077
# tar cvfz /backupdir/$(date +%Y%m%d)-ca-backup.tgz /data/
----
CAUTION: The backup contains the unencrypted private keys of your
certification authority. It should be stored in a secure place.
--
Encrypted ZIP::
+
--
An encrypted backup can be created using the `7z(1)` commmand, you'll be
prompted to enter an encryption password.

[source,shell]
----
# umask 077
# 7z a -mhe=on -p /backupdir/$(date +%Y%m%d)-ca-backup.7z /data/
----
--
====

== Restore

Stop the instance you're backup up first (kbd:[Ctrl-C] or send
signal `TERM` to the `node-red` process).

[tabs]
====
Simple tar::
+
--
Extract the most recent backup file afterwards with `tar(1)` using the
following command:

[source,shell]
----
# tar xvfz /backupdir/20200930-ca-backup.tgz -C /data/
----
--
Encrypted ZIP::
+
--
Extract the most recent backup file afterwards with `7z(1)` using the
following command:

[source,shell]
----
# 7z x -p /backupdir/20200930-ca-backup.7z
# chmod -R 750 /data/*
# chown -R node-red:node-red /data/*
----
--
====

