= Monitoring

The server comes with integrated performance metrics. Please see the
following sections how to integration them with your monitoring solution.

== Metrics

The certification authority service comes with build in metrics. They
are available unter http://${HOSTNAME}:1880/metrics/. See the description of
the https://prometheus.io/docs/concepts/data_model/[file format] of this
endpoint. To see which metrics are provided you can execute the following
command on your certification authority node.

[source,shell]
----
$ curl http://localhost:18080/metrics
----

To protect the metrics of your instance the following environment
variables can be set:

- `METRICS_USERNAME`, and
- `METRICS_PASSWORD`

When set, these values have to be specified using HTTP basic
authentication when querying metrics.

== Integration

The metrics can be read-out with https://prometheus.io/[Prometheus]
or with https://opennms.org[OpenNMS] on regular intervals. Both systems
are backed by a time-series database to store all performance metrics
gathered. These time-series can be visualized by https://grafana.org[
Grafana].

The following figure shows how the different components can be used
for monitoring:

.Monitoring metrics overview
[ditaa]
....
                          +-------------+
                          |             |
                          | node-red CA |
                          |             |
                          +-------------+
                                 ^
                    GET /metrics |
                                 |
+-------------+           +-------------+
|             |  http(s)  |             |
|   grafana   |---------->|  prometheus |
|             |           |             |---+
+-------------+           +-------------+   |
       ^                          |         | OpenNMS
       |                          |         |
       | https                    +---------+
       |
       |
+-------------+
|             |
| Web-Browser |
|             |
+-------------+
....

The following sections describe how-to setup monitoring with these
systems.

=== Prometheus

The following paragraph shows a (very basic) installation of prometheus
and how to configure it to scrape the certification authority.

==== Installation

The prometheus software can be installed using the following commands:

[tabs]
====
FreeBSD::
+
--
[source,shell]
----
# pkg install prometheus
----
--
====

==== Configuration

The configuration can be found under `/usr/local/etc/prometheus.yml`
Add the following job under `scrape_configs`.

NOTE: The `basic_auth` entry is optional and only needed if you have
started your instance with `METRICS_USERNAME` and `METRICS_PASSWORD`
environment variables.

.prometheus.yml
[source]
----
scrape_configs:
  - job_name: 'certification authority'
    static_configs:
    - targets: ['localhost:1880']
      basic_auth:
        username: prometheus
        password: password
----

==== Startup

Enable automatic startup and start the server for the first time:

[tabs]
====
FreeBSD::
+
--
[source]
----
# sysrc prometheus_enable=YES
# /usr/local/etc/rc.d/prometheus start
----
--
====

The server can be reached under http://localhost:9090/.

=== Grafana

==== Installation

For prometheus the most recent grafana package can be used:

[tabs]
====
FreeBSD::
+
--
[source,shell]
----
# pkg install grafana7
----
--
====

OpenNMS relies on an older version of grafana:

[tabs]
====
FreeBSD::
+
--
[source,shell]
----
# pkg install grafana6
----
--
====

==== Startup

Enable automatic startup and start the server for the first time:

[tabs]
====
FreeBSD::
+
--
[source,shell]
----
# sysrc grafana_enable=YES
# /usr/local/etc/rc.d/grafana start
----
--
====

==== Configuration

The configuration is done via http://localhost:3000[the web-frontend of
grafana]. You're prompted for a new `admin` password, when you login the
first time.

==== Create a datasource

At first you'll have to create a datasource pointing to either
http://localhost:9090[Prometheus] or to http://localhost:8980[OpenNMS].
The datasource plugin for prometheus is provided by the default
installation. The datasource plugin for OpenNMS is installed with the
helm plugin.

[source,shell]
----
# grafana-cli plugin install opennms-helm-app
----

==== Create your dashboard

Create a new dashboard and choose your datasource created in the
paragraph before. Then you'll have to add the time-series you want
to monitor.

== Advanced topics

After setting up either prometheus or OpenNMS, and grafana, the next
step is to configure an alerting mechanism to inform you, whenever
something went out-of-bounds. This is beyond the scope of this guide.
Prometheus relies on
https://prometheus.io/docs/alerting/latest/alertmanager/[Alertmanager],
while OpenNMS comes with its own alerting mechanisms. For both
monitoring solutions sketched here it is possible to use the alerting
mechanisms embedded in grafana as well.

